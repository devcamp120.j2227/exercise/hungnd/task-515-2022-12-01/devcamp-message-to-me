import React, { Component } from "react";
import TitleImage from "./image/TitleImage";
import TitleText from "./text/TitleText";

class Title extends Component {
    render() {
        return(
            <React.Fragment>
                <TitleText/>
                <TitleImage/>
            </React.Fragment>
        )
    }
}
export default Title;