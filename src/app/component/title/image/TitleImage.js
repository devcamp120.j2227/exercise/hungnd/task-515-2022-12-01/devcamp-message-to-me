import { Component } from "react";

import programing from '../../../../assets/images/programing.png';

class TitleImage extends Component {
    render() {
        return (
            <div className='row mt-3'>
                <div className='col-12'>
                    <img src={programing} alt="background" style={{ width: "50%" }} />
                </div>
            </div>
        )
    }
}
export default TitleImage;