import React, { Component } from "react";

import likePic from '../../../../assets/images/like.png';

class LikeImage extends Component {
    render() {
        const { outputMessageProp, showLikeImangeProp } = this.props;

        return (
            <React.Fragment>
                <div className='row mt-3'>
                    <div className='col-12'>
                        {outputMessageProp.map((value, index) => {
                            return(
                                <p key={index}>{value}</p>
                            )
                        })}
                    </div>
                </div>
                <div className='row mt-3 mb-5'>
                    <div className='col-12'>
                        {showLikeImangeProp ? <img src={likePic} alt="imageLike" width={150} /> : null}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default LikeImage;