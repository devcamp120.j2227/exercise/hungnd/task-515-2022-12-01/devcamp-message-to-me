import React, { Component } from "react";

import InputMessage from "./input-message/InputMessage";
import LikeImage from "./like-image/LikeImage";

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputMessage: "",
            outputMessage: [],
            showLikeImage: false
        }
    }
    // Tạo 1 hàm cho phép thay đổi inputMessage
    inputMessageChangeValue = (value) => {
        this.setState({
            inputMessage: value
        })
    }
    // Tạo 1 hàm cho phép đổi state outputMessage và showLikeImage
    outputMessageChangeValue = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                showLikeImage: true
            })
        }
    }
    render() {
        return (
            <React.Fragment>
                <InputMessage inputMessageProp={this.state.inputMessage} inputMessageChangeValueProp={this.inputMessageChangeValue} outputMessageChangeValueProp={this.outputMessageChangeValue}/>
                <LikeImage outputMessageProp={this.state.outputMessage} showLikeImangeProp={this.state.showLikeImage} />
            </React.Fragment>
        )
    }
}
export default Content;