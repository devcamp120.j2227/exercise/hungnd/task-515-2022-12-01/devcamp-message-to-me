import React, { Component } from "react";

class InputMessage extends Component {

    onBtnClickHandler = () => {
        const { outputMessageChangeValueProp } = this.props;
        outputMessageChangeValueProp();
    }

    onChangeInputHandler = (event) => {
        const { inputMessageChangeValueProp } = this.props;
        inputMessageChangeValueProp(event.target.value);
    }
    render() {
        const { inputMessageProp } = this.props;

        return (
            <React.Fragment>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <label>Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <input className='input-message' placeholder='Nhập message' onChange={this.onChangeInputHandler} value={inputMessageProp} />
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <button className='btn btn-success' style={{ width: "300px" }} onClick={this.onBtnClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>
            </React.Fragment>

        )
    }
}
export default InputMessage;