// Import thư viện bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

import Content from './app/component/content/Content';
import Title from './app/component/title/tittle';

function App() {
  return (
    <div className='container mt-5 text-center'>
      <Title />
      <Content />
    </div>
  );
}

export default App;
